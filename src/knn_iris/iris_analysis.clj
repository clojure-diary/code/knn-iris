(ns knn-iris.iris-analysis
  (:use csv-map.core)
  (:require [knn-iris.lp-distance :as lpd]))

(defn column-to-number [data column]
  (map #(assoc % column (Double/parseDouble (% column))) data))

(def data (-> "iris.csv"
              (slurp)
              (parse-csv)
              (column-to-number "sepal_length")
              (column-to-number "sepal_width")
              (column-to-number "petal_length")
              (column-to-number "petal_width")))

(defn data-with-distance [point data]
  (let [petal-length (first point)
        sepal-length (second point)]
    (map #(assoc % :distance
                 (lpd/vector-distance
                  [petal-length sepal-length]
                  [(% "petal_length") (% "sepal_length")]))
         data)))

(take 5 (sort-by  :distance (data-with-distance [1.4 5.1] data)))
(take 5 (sort-by  :distance (data-with-distance [5 6] data)))
(take 5 (sort-by  :distance (data-with-distance [4.2 5.5] data)))

(->> (data-with-distance [4 5.9] data)
     (sort-by :distance)
     (take 5)
     (map #(get % "species"))
     (frequencies)
     (sort-by val)
     (last)
     (first))

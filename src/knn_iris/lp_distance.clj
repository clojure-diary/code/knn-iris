;; lp_distance.clj

(ns knn-iris.lp-distance)

(defn absolute-distance [s1 s2]
  (Math/abs (- s1 s2)))

(defn lp-distance [v1 v2 p]
  (as-> (map absolute-distance v1 v2) x
    (map #(Math/pow % p) x)
    (reduce + x)
    (Math/pow x (/ 1 p))))

(defn vector-distance [v1 v2]
  (lp-distance v1 v2 2))

(defn manhattan-distance [v1 v2]
  (lp-distance v1 v2 1))
